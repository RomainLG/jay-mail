<?php
/*
 *  Controlleur de connexion
 */
require_once("../config/config.php");
$squelette = "../ui/pages/signin.html.php";
$titre = "Super Mail : Connexion";
$login = "";
//Vérification de connexion
session_start("mail");
$auth = Auth::getInstance();
try {
    if (isset($_POST['form_login'])) {
        $login = $_POST['form_login'];
        $auth->verifierAuthentification($_POST['form_login'], $_POST['form_pass']);
    }
    if ($auth->estConnecte()) {
        header("Location:" . BASE_URL . "public/index.php");
    }
} catch (Exception $exc) {
    //TODO : afficher les erreurs dans le formulaire
}

ob_start();
require_once($squelette);
$html = ob_get_contents();
ob_end_clean();
echo $html;
?>