<?php

class Outils_Chaines {
  /**
   * quotes2entity replaces all " in the string by &quot;
   * It is to be used in all HTML attributes such as title, longdesc,
   * value in a form, etc.
   *
   * @param $string the string to recode
   * @return transformed string
   */
  public static function quotes2entity($string) {
    return str_replace("\"", "&quot;", $string);
  }

  /**
   * htmlEncodeString replaces all <, > and & by their HTML entities
   * @param $string string to encode
   * @param $charset character encoding, optionnal (defaults to utf-8)
   *
   * @return encoded tring
   */
  public static function htmlEncodeString($string, $charset = "utf-8") {
    return trim(htmlspecialchars($string, ENT_NOQUOTES, $charset));
  }

  /**
   * htmlEncodeArray recursively loops an array to encode <, > and & in
   * all entries (typically a POST data array)
   *
   * @param $array passed by reference array
   * @param $htmlOk list of array keys where HTML is accepted, hence
   * where no encoding is needed
   *
   * @note no return value since array is passed by reference
   */
  public static function htmlEncodeArray(& $tab, $htmlOk = array()) {
    while (list($k, $v) = each($tab)) {
      if (!is_array($tab[$k])) {
	if (!in_array($k, $htmlOk)) {
	  $tab[$k] = Outils_Chaines::htmlEncodeString($tab[$k]);
	}
      } else {
	if (in_array($k, $htmlOk)) {
	  $htmlOk = array_merge($htmlOk, array_keys($tab[$k]));
	}
	self::htmlEncodeArray($tab[$k], $htmlOk);
      }
    }
  }



  	/*
  	|------------------------------------------------------------------
  	| Coupe une chaîne après 'n' caractères
  	|------------------------------------------------------------------
  	|
  	*/
	public static function truncate($text, $length = 100, $idArticle, $ending = '...', $exact = false, $considerHtml = true) {
	if ($considerHtml) {
		// if the plain text is shorter than the maximum length, return the whole text
		if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
			return $text;
		}
		// splits all html-tags to scanable lines
		preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
		$total_length = strlen($ending);
		$open_tags = array();
		$truncate = '';
		foreach ($lines as $line_matchings) {
			// if there is any html-tag in this line, handle it and add it (uncounted) to the output
			if (!empty($line_matchings[1])) {
				// if it's an "empty element" with or without xhtml-conform closing slash
				if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
					// do nothing
				// if tag is a closing tag
				} else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
					// delete tag from $open_tags list
					$pos = array_search($tag_matchings[1], $open_tags);
					if ($pos !== false) {
					unset($open_tags[$pos]);
					}
				// if tag is an opening tag
				} else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
					// add tag to the beginning of $open_tags list
					array_unshift($open_tags, strtolower($tag_matchings[1]));
				}
				// add html-tag to $truncate'd text
				$truncate .= $line_matchings[1];
			}
			// calculate the length of the plain text part of the line; handle entities as one character
			$content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
			if ($total_length+$content_length> $length) {
				// the number of characters which are left
				$left = $length - $total_length;
				$entities_length = 0;
				// search for html entities
				if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
					// calculate the real length of all entities in the legal range
					foreach ($entities[0] as $entity) {
						if ($entity[1]+1-$entities_length <= $left) {
							$left--;
							$entities_length += strlen($entity[0]);
						} else {
							// no more characters left
							break;
						}
					}
				}
				$truncate .= substr($line_matchings[2], 0, $left+$entities_length);
				// maximum lenght is reached, so get off the loop
				break;
			} else {
				$truncate .= $line_matchings[2];
				$total_length += $content_length;
			}
			// if the maximum length is reached, get off the loop
			if($total_length>= $length) {
				break;
			}
		}
	} else {
		if (strlen($text) <= $length) {
			return $text;
		} else {
			$truncate = substr($text, 0, $length - strlen($ending));
		}
	}
	// if the words shouldn't be cut in the middle...
	if (!$exact) {
		// ...search the last occurance of a space...
		$spacepos = strrpos($truncate, ' ');
		if (isset($spacepos)) {
			// ...and cut the text in this position
			$truncate = substr($truncate, 0, $spacepos);
		}
	}
	// add the defined ending to the text
	$truncate .= $ending . '<br /><br /><a href="'.PUBLIC_URL.'index.php?a=permalien&id='.$idArticle.'">Lire la suite</a>';
	if($considerHtml) {
		// close all unclosed html-tags
		foreach ($open_tags as $tag) {
			$truncate .= '</' . $tag . '>';
		}
	}
	return $truncate;
}


}
?>