<?php

class Outils_Bd {
  private static $instance;

  protected $connexion;

  private function __construct() {
      $this->connexion = new PDO(PDO_DSN, USER, PASSWD);
      $this->connexion->query("SET NAMES utf8");
      $this->connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  private function __clone() {}

  static public function getInstance() {
    if (! (self::$instance instanceof self)) {
      self::$instance = new self();
    }
    return self::$instance;
  }

  public function getConnexion() {
    return $this->connexion;
  }
}

?>