<?php

class Mail_Inbox extends Zend_Mail_Storage_Imap{    
    
    protected $_messageClass = 'Mail_Message';

    /*
     * Zend ne permettant pas la recherche, cette method patch :
     * 
     */
    public function search($req)
    {
        return $this->_protocol->search(array($req));
    }
}

?>
