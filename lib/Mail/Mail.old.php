<?php

class Mail {

    public static function getMailsJson($prov, $user, $pwd) {
        /* try to connect */
        $inbox = imap_open($prov, $user, $pwd) or die('MESSAAAAAAAAAAAGE : ' . imap_last_error());

        /* grab emails */
        $emails = imap_search($inbox, 'ALL');
        $json = "";
        /* if emails are returned, cycle through each... */
        if ($emails) {
            $ms = array();
            /* begin output var */
            $output = '';

            /* put the newest emails on top */
            rsort($emails);

            /* for every email... *
            for ($i = 0; $i < 20; $i++) {
                $email_number = $emails[$i];*/
            foreach ($emails as $email_number) {
                $m = array();

                /* get information specific to this email */
                $overview = imap_fetch_overview($inbox, $email_number, 0);

                $m['toggler'] = ($overview[0]->seen ? 'read' : 'unread');
                $m['subject'] = $overview[0]->subject;
                $m['from'] = $overview[0]->from;
                $m['date'] = $overview[0]->date;
                $m['body'] = imap_fetchbody($inbox, $email_number, 2);
                $ms[] = $m;
            }

            $json = json_encode($ms);
            //var_dump($json);
        }
        /* close the connection */
        imap_close($inbox);
        return $json;
    }
    
    
    
    public static function getMails($prov, $user, $pwd) {
        /* try to connect */
        $inbox = imap_open($prov, $user, $pwd) or die('MESSAAAAAAAAAAAGE : ' . imap_last_error());

        /* grab emails */
        $emails = imap_search($inbox, 'ALL');
        $ms = array();
        /* if emails are returned, cycle through each... */
        if ($emails) {
            /* put the newest emails on top */
            rsort($emails);
            /* for every email... *
            for ($i = 0; $i < 20; $i++) {
                $email_number = $emails[$i];*/
            foreach ($emails as $email_number) {
                $m = array();
                /* get information specific to this email */
                $overview = imap_fetch_overview($inbox, $email_number, 0);

                $m['toggler'] = ($overview[0]->seen ? 'read' : 'unread');
                $m['subject'] = $overview[0]->subject;
                $m['from'] = $overview[0]->from;
                $m['date'] = $overview[0]->date;
                $m['body'] = imap_fetchbody($inbox, $email_number, 2);
                $ms[] = $m;
            }
            //var_dump($json);
        }
        /* close the connection */
        imap_close($inbox);
        return $ms;
    }

}

?>