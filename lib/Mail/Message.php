<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mail_Message
 *
 * @author R
 */
class Mail_Message extends Zend_Mail_Message {

    /**
     * Return the text part of the message (in UTF-8)
     *
     * @return string
     */
    public function getText() {
        $contentTransfertEncoding = "";
        
        if ($this->isMultipart()) {
            foreach (new RecursiveIteratorIterator($this) as $part) {
                try {
                    if (strtok($part->contentType, ';') == 'text/plain') {
                        $text = $part->getContent();

                        if ($part->contentTransferEncoding) {
                            $contentTransfertEncoding = $part->contentTransferEncoding;
                        }
                    }
                } catch (Zend_Mail_Exception $e) {
                    // ignore
                }
            }
        } else {
            $contentTransfertEncoding = $this->contentTransferEncoding;
            if (preg_match('/charset="(.+)"$/', $this->contentType, $matches)) {
                $charset = $matches[1];
            }
            $text = $this->getContent();
        }
        // Decode the content
        switch ($contentTransfertEncoding) {
            case 'base64':
                $text = base64_decode($text);
                break;

            case '7bit':
            case 'quoted-printable':
                $text = quoted_printable_decode($text);
                break;

            default:
                //throw new Exception("Unknown encoding: '{$contentTransfertEncoding}'");
        }

        // Convert to UTF-8 if necessary
        if (!isset($charset) or ($charset != 'UTF-8')) {
            $text = utf8_encode($text);
        }

        return $text;
    }
    
    private function toUtf8($str){
        if ( true )
        {
            echo "coucou";
            return utf8_encode($str);
        }
        return $str;
    }
    
    public function getFrom() {
        return $this->toUtf8($this->from);
    }
}
?>
