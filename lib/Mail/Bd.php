<?php

/**
 * Description of _MailDb
 *
 * @author R
 */
class Mail_Bd {
    
    static public function saveRqst($tab) {
        //Enregistre une requete en BDD
        $db = Outils_Bd::getInstance()->getConnexion();
        $sth = $db->prepare("insert into requetes set nom=:name,
                    uid=:uid,
                    req=:req,
                    field_from=:from_mail,
                    field_unseen=:unseen,
                    field_since=:since,
                    field_until=:until");
        $data = array(
            "name" => $tab["name"],
            "uid" => $tab["uid"],
            "req" => $tab["req"],
            "from_mail" => $tab["from_mail"],
            "unseen" => $tab["unseen"],
            "since" => $tab["since"],
            "until" => $tab["until"]
        );
        $sth->execute($data);
    }
    
    static public function getRqsts($uid){
        //Récupération des requètes enregistrée par l'utilisateur donné
        $db = Outils_Bd::getInstance()->getConnexion();
        $sth = $db->prepare("SELECT * FROM requetes where uid=:uid");
        $data = array('uid' => $uid);
        $sth->execute($data);
        return $sth->fetchall();
    }
    
    static public function deleteRqst($id, $uid){        
        //Suppression d'une requète enregistrée
        $db = Outils_Bd::getInstance()->getConnexion();
        $sth = $db->prepare("DELETE FROM requetes WHERE id=:id AND uid=:uid");
        $data = array(
            'id' => $id,
            'uid' => $uid);
        $sth->execute($data);
    }
}

?>
