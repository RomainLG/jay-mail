<?php
/**********************************************************************
 *    Classe : Auth _Ui                                               *
 *    Auteur : JML jml@info.unicaen.fr                                *
 *    Date : Janvier 2008                                             *
 *    Objet : gérer les affichages pour l'authentification            *
 *                                                                    *
 **********************************************************************/

class Auth_Ui {

  protected $auth;

  /**
   * le constructeur prend l'objet Auth en paramètre
   */
  public function __construct(Auth  $auth) {
    $this->auth = $auth;
  }

  /**
   * Méthode afficherInformationsConnexion
   * Affiche les informations de connexion et le lien quitter
   */
  public function afficherInformationsConnexion() {
    $html = "<div id=\"infosConnexion\">Bonjour {$this->auth->getPrenom()} {$this->auth->getNom()}<br />Vous êtes {$this->auth->getStatut()}</div>\n";
    $html .= "<div id=\"quitter\"><a href=\"" . BASE_URL . "public/index.php?a=quitter\">Quitter</a></div>";
    return $html;
  }


  /**
   * Méthode afficherFormulaireConnexion
   * Affiche le formualire de connexion
   * Prend en paramètre l'URL de l'action
   */
  public function afficherFormulaireConnexion($urlAction) {
    $html = <<<EOT
<form action="{$urlAction}" method="post">
<div>
	<input type="text" placeholder="Pseudo" name="auth_login" value="" size="8" /><br />
	<input type="password" placeholder="Mot de passe" name="auth_pass" value="" size="8" /><br />
<input type="submit" name="envoi" value="Envoi" />
</div>
<p class="yahoo social">
	<span class="icon-yahoo"></span>
	<a href="?provider=Yahoo">Connexion Yahoo!</a>
</p>
<p class="facebook social">
	<span class="icon-facebook"></span>
	<a href="?provider=facebook">Connexion Facebook</a>
</p>
<p class="twitter social">
	<span class="icon-twitter"></span>
	<a href="?provider=twitter">Connexion Twitter</a>
</p>
<p class="google social">
	<span class="icon-google"></span>
	<a href="?provider=google">Connexion Google</a>
</p>
</form>
EOT;

    return $html;
  }

} // fin classe Auth_Ui



?>
