<?php

class Auth {

    protected static $auth = null;
    protected $infosAuthentification = array();

    private function __construct() {
        if (isset($_SESSION['infosAuthentification'])) {
            $this->infosAuthentification = $_SESSION['infosAuthentification'];
        } else {
            $this->infosAuthentification = array();
        }
    }

    private function __clone() {
        
    }

    public static function getInstance() {
        if (null === self::$auth) {
            self::$auth = new self();
        }
        return self::$auth;
    }

    public function verifierAuthentification($login, $pass) {
        /* $yahoo = '{imap.mail.yahoo.com:993/imap/ssl}INBOX';
          $aol = '{imap.aol.com:993/imap/ssl}INBOX'; */
        $endmail = explode("@", $login);
        $provider = $endmail[1];
        $costring = "";
        switch ($provider) {
            case "gmail.com":
                $costring = '{imap.gmail.com:993/imap/ssl}INBOX';
                break;
            case "etu.unicaen.fr":
                $costring = '{imap.unicaen.fr:993/imap/ssl}INBOX';
                break;
            default:
                throw new Auth_Exception("Fournisseur non supporté");
                break;
        }
        if ($mbox = imap_open($costring, $login, $pass)) {
            imap_close($mbox);
            $this->infosAuthentification['id'] = $this->synchroDb($login);
            $this->infosAuthentification['login'] = $login;
            $this->infosAuthentification['pass'] = $pass;
            $this->infosAuthentification['provider'] = $provider;
            $this->synchroniser();
        } else {
            throw new Auth_Exception("Login ou mot de passe incorrect");
        }
    }

    private function synchroDb($mail) {
        //Ajoute l'utilisateur en db lors de sa première visite
        //retourne son ID
        if (!$this->getUserId($mail)){
            //Première visite
            $this->addUser($mail);
        }
        return $this->getUserId($mail);
    }

    function getUserId($mail) {
        $db = Outils_Bd::getInstance()->getConnexion();
        $sth = $db->prepare("SELECT id FROM users where mail=:mail");
        $data = array('mail' => $mail);
        $sth->execute($data);
        $res = $sth->fetch();
        if ($res){
            return $res['id'];
        }
        return false;
    }

    function addUser($mail) {
        /* Ajour d'un utilisateur en db */
        $db = Outils_Bd::getInstance()->getConnexion();
        $sth = $db->prepare("insert into users set mail=:mail");
        $data = array(
            'mail' => $mail
        );
        $sth->execute($data);
    }

    public function estConnecte() {
        return !empty($this->infosAuthentification);
    }

    public function getLogin() {
        return $this->infosAuthentification['login'];
    }

    public function getPass() {
        return $this->infosAuthentification['pass'];
    }

    public function getProvider() {
        return $this->infosAuthentification['provider'];
    }

    public function getId() {
        return $this->infosAuthentification['id'];
    }

    public function quitter() {
        $this->infosAuthentification = array();
        $this->synchroniser();
        session_destroy();
    }

    private function synchroniser() {
        $_SESSION['infosAuthentification'] = $this->infosAuthentification;
    }

}

?>