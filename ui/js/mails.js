/*
 * Définir la requete associee a la vue active
 */
function setRequest(reqst, page){
    $("#"+active_view).attr("mail-reqst", reqst);
    $("#"+active_view).attr("mail-page", page);
    doFirstSeach(active_view);
}

/*
 *Lancer une requète sur la  (transparent, pour refresh)
 */
function doSeach(view) {
    var idView = "#" + view;
    //On récupère la requète en attribut :
    reqst = $(idView).attr("mail-reqst");
    page = $(idView).attr("mail-page");
    $.ajax({
        type: "GET",
        url: "mails_controler.php",
        data: {a: 'recherche', req: reqst, page: page},
        async: true,
        dataType: "json",
        success: function(data) {
            $(idView + " .loading_mails").remove();
            $(idView + " .pagination").remove();
            $(idView + " .list_mail table tbody").empty();
            mailsHtml(idView, data.msgs);
            paginatorHtml(view, Math.ceil(data.page_max), page);
        }
    });
}
/*
 *Lancer une première requète sur la vue (affichage du loader)
 */
function doFirstSeach(view) {
    var idView = "#" + view;
    //On récupère la requète en attribut :
    reqst = $(idView).attr("mail-reqst");
    page = $(idView).attr("mail-page");
    $.ajax({
        type: "GET",
        url: "mails_controler.php",
        data: {a: 'recherche', req: reqst, page: page},
        async: true,
        beforeSend: function(x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/j-son;charset=UTF-8");
            }
            $(idView + " .loading_mails").remove();
            $(idView + " .pagination").remove();
            $(idView + " .list_mail table tbody").empty();
            $(idView + " .list_mail").append("<img class='loading_mails' alt='loading...' src='../ui/img/spin.png' />");
        },
        dataType: "json",
        success: function(data) {
            $(idView + " .list_mail table tbody").empty();
            mailsHtml(idView, data.msgs);
            paginatorHtml(view, Math.ceil(data.page_max), page);
        },
        complete: function() {
            $(idView + " .loading_mails").remove();
        },
        error: function() {
            $(idView + " .list_mail table tbody").append("<tr><td>Aucun message trouvé !</td></tr>");
        }
    });
}

/*
 * Affichage d'une liste de mails au format HTML
 */
function mailsHtml(idView, data) {    
    var html = "";
    $.each(data, function(i, mail) {
        html += "<tr id='mail" + mail.number + "' data-toggle='modal' data-target='#mailModal' \n\
onclick='openMail(\"#mail" + mail.number + "\", " + mail.number + ")' class='active";
        if (mail.seen == "false")
            html += " unseen";
        html += "'><td>De : " + mail.from + "</td>";
        html += "<td>" + mail.subject + "</td>";
        html += "<td>" + mail.date + "</td></tr>";
    });
    $(idView + " .list_mail table tbody").append(html);
}

/*
 * Construction de la pagination pour une vue donnée
 */
function paginatorHtml(idView, page_max, page){
    html = "<ul view='"+idView+"' id='pagi-" + idView + "' class='pagination pull-right'>"; 
    min = Math.max(1, page - 2);
    max = Math.min(page_max, page + 2);
    for(i = min; i <= max; i++){        
        html += "<li";
        if(page == i)
            html += " class='active'";
        html += "><a href='#'>" + i + "</a></li>"
    }        
    html += "</ul>";
    $("#"+idView + " .list_mail").append(html);
    
    //Pagination en javascript :
    paginator = $("#pagi-"+idView);
    view = $("#" + paginator.attr("view"));
    $("#pagi-"+idView+" li").click(function(evt){        
        evt.preventDefault();
        if (this.className != "active"){ 
            //On n'a pas clické sur la page actuelle -> chagement
            view.attr("mail-page", $(this).text());
            doFirstSeach(idView);
        }
    });
}

/*
 * Récupère le nombre de mails non lus
 */
function countUnseen() {
    $.ajax({
        type: "GET",
        url: "mails_controler.php",
        data: {a: 'count'},
        async: true,
        success: function(data) {
            $("#count_unseen").empty();
            $("#count_unseen").append(data);
        }
    });
}

/*
 * Vérifier régulièrement dans chaque vue si de nouveaux messages ont été raçus
 */
function refreshAllViews(){
    setInterval(function(){
        $.each(list_of_views, function(){
            doSeach(this);
        })
    }, 10000);
}

function formatDate(dateInput) {
    var mois = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    var elem = dateInput.split('-');
    j = elem[2];
    m = elem[1];
    a = elem[0];
    return j + "-" + mois[parseInt(m) - 1] + "-" + a;
}


/*
 * Affichage d'un e-mail dans une modale
 */
function openMail(idligne, num) {
    $(idligne).removeClass("unseen");
    $.ajax({
        type: "GET",
        url: "mails_controler.php",
        data: {a: 'lire', num: num},
        async: true,
        dataType: "json",
        beforeSend: function(x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/j-son;charset=UTF-8");
            }
            $("#mailModal div.modal-header h4.modal-title").empty();
            $("#mailModal div.modal-header h4.modal-title").append("Chargement ...");
            $("#mailModal div.modal-header h5.modal-fromaddress span").empty();
            $("#mailModal div.modal-header h5.modal-toaddress span").empty();
            $("#mailModal div.modal-header h5.modal-date span").empty();
            $("#mailModal div.modal-body").empty();
            $("#mailModal div.modal-body").append("<img class='loading_mails' alt='loading...' src='../ui/img/spin.png' />");
        },
        success: function(data) {
            $("#mailModal div.modal-header h4.modal-title").empty();
            $("#mailModal div.modal-header h4.modal-title").append(data.subject);
            $("#mailModal div.modal-header h5.modal-fromaddress span").empty();
            $("#mailModal div.modal-header h5.modal-fromaddress span").append(data.from);
            $("#mailModal div.modal-header h5.modal-toaddress span").empty();
            $("#mailModal div.modal-header h5.modal-toaddress span").append(data.to);
            $("#mailModal div.modal-header h5.modal-date span").empty();
            $("#mailModal div.modal-header h5.modal-date span").append(data.date);
            $("#mailModal div.modal-body").empty();
            $("#mailModal div.modal-body").append(data.body);
        },
        complete: function() {
        },
        error: function() {
            console.log("oops!");
        }
    });
}


/* Gestion des vues */
function addCol() {
    nb_view++;
    nb_col++;
    $.ajax({
        type: "GET",
        url: "../ui/fragments/list_mails.html",
        success: function(data) {
            html = "<div class='colonne' id='colX'>";
                //html += "<div class='ui-resizable' style='position: relative;'>";
                    html += "<div class='col-header'>";
                        html += "<button title='Supprimer la colonne' class='col-deleter close close-col'>&times;</button>";
                        html += "<button title='Ajouter un ligne dans cette colonne' onclick='addViewInCol(\"#col" + nb_col + "\")' class='close close-col'>+</button>";
                    html += "</div>"; //fin col-header
                    //Liste des mails :
                    html += data;
                //html += "</div>"; //fin ui-resizable
            html += "</div>"; //fin colonne
            $("#lists_container").append(html);

            $("#viewX .deleter").attr('onclick', 'removeView("view' + nb_view + '")'); //suppression de la vue
            $("#colX .col-deleter").attr('onclick', 'removeCol("col' + nb_col + '")'); //suppression de la colonne
            //identification du formulaire :
            $("#viewX").attr('onclick', 'setActive("view' + nb_view + '")');
            $("#viewX").attr('id', 'view' + nb_view);
            $("#colX").attr('id', 'col' + nb_col);
            //On ajoute la vue à la liste des vues à rafraichir
            list_of_views.push('view' + nb_view);
            setActive('view' + nb_view);
            setRequest("ALL", 1);
            setResizable();
        },
        error: function() {
            console.log("Erreur lors de l'ajout de la vue !");
        }
    });
}

function setResizable() {
    $('.colonne').resizable({
        handles: 'e'
    });
}

function addViewInCol(idCol) {
    nb_view++;
    $.ajax({
        type: "GET",
        url: "../ui/fragments/list_mails.html",
        success: function(data) {
            $(idCol).append(data);
            //identification du formulaire :
            $("#viewX .deleter").attr('onclick', 'removeView("view' + nb_view + '")');
            $("#viewX").attr('onclick', 'setActive("view' + nb_view + '")');
            $("#viewX").attr('id', 'view' + nb_view);
            //On ajoute la vue à la liste des vues à rafraichir
            list_of_views.push('view' + nb_view);
            console.log(list_of_views);
            setActive('view' + nb_view);
            setRequest("ALL", 1);
        },
        error: function() {
            console.log("Erreur lors de l'ajout de la vue !");
        }
    });
}


/*
 * Supprimer une vue de l'affichage et de la liste des vues
 */
function removeView(view) {
    index = jQuery.inArray( view, list_of_views );
    list_of_views.splice(index, 1);
    id = "#" + view;
    $(id).remove();
}

/*
 * Supprimer une colonne est toutes ses vues.
 */
function removeCol(col){
    $("#" + col + " .ligne").each(function(){
        removeView(this.id);
    });
    $("#" + col).remove();
}


function setActive(view) {
    //Suppression de la bordure sur la précdédente vue active
    $(".view").each(function() {
        $(this).removeClass("active-view");
    });
    id = "#" + view;
    //Ajout de la bordure sur la nouvelle vue active
    $(id).addClass("active-view");
    active_view = view;
}



/*
 * Fait en sorte que le formulaires de recherche fonctionnent en
 * AJAX, annulation de la requèe PHP
 */
function formToAJAX() {
    $('.search_mail').submit(function(evt) {
        evt.preventDefault();
        id_view = "#" + active_view;

        //Lecture du formulaire de recherche
        from_mail = $("#expediteur").val();
        unseen = false;
        if (($("input[name=unseen]:checked").map(function() {
            return this.value;
        }).get().join(",")) == "true") {
            unseen = true;
        }
        since = $("#since").val();
        until = $("#until").val();

        //Construction de la requète IMAP
        req = buildRequest(from_mail, unseen, since, until);

        //Définir la requète pour la vue active
        setRequest(req, 1);
        //showMails(req, page);
    });
}

function buildRequest(from_mail, unseen, since, until) {
    req = "ALL";
    if (from_mail != "" || unseen || since != "" || until != "") {
        if (unseen)
            req += " UNSEEN";
        if (from_mail != "")
            req += " FROM " + from_mail;
        if (since != "")
            req += " SINCE " + formatDate(since);
        if (until != "")
            req += " BEFORE " + formatDate(until);
    }
    return req;
}

function saveRequest() {
    //Lecture du formulaire
    name = $("#saved-request-name").val();
    from_mail = $("#expediteur").val();
    unseen = false;
    if (($("input[name=unseen]:checked").map(function() {
        return this.value;
    }).get().join(",")) == "true") {
        unseen = true;
    }
    since = $("#since").val();
    until = $("#until").val();

    //Construction de la requète IMAP
    req = buildRequest(from_mail, unseen, since, until);

    //Enregistrement de la requète
    $.ajax({
        type: "GET",
        url: "mails_controler.php",
        data: {a: 'enregistrer_req',
            name: name,
            req: req,
            from_mail: from_mail,
            unseen: unseen,
            since: since,
            until: until
        },
        async: true,
        dataType: "json",
        success: function(data) {
            console.log("OK !");
            html = "";
            html += "<div class='alert " + data.alert + " alert-dismissable'>";
            html += "<button type='button' class='close' data-dismiss='alert'\n\
                     aria-hidden='true'>&times;</button>";
            html += data.msg + "</div>";
            $("#top-alerts").append(html);
            getRequests();
        },
        complete: function() {
        },
        error: function() {
            alert("Erreur lors de l'enregistrement");
        }
    });
}


function getRequests() {
    //Récupération des requète enregisrtées par l'utlisateur
    $.ajax({
        type: "GET",
        url: "mails_controler.php",
        data: {a: 'list_req'},
        async: true,
        dataType: "json",
        success: function(data) {
            console.log(data);
            $("#list-requests").empty();
            //On affiche les requetes dans la sidebar :                
            var html = "";
            $.each(data, function(i, req) {
                html += "<tr class='active'><td onclick='setRequest(\""
                        + req.req + "\",1)'>";
                html += req.nom + "</td><td>";
                html += "<button type='button' class='close' \n\
                      onclick='deleteSavedReq(" + req.id + ")'";
                html += ">&times;</button></td></tr>";
            });
            $("#list-requests").append(html);
        },
        error: function() {
            console.log("Erreur lors de la récupération des requètes.");
        }
    });
}

function deleteSavedReq(id) {
    //alert(id);
    alert = "<div class='alert alert-info fade in'>";
    alert += "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
    alert += "<h4>Confirmation</h4>";
    alert += "<p>Vous êtes sur le point de supprimer une requète enregistrée</p>";
    alert += "<p>Voulez-vous continuer ?</p>";
    alert += "<button type='button' class='btn btn-success' onclick='doDelete(" + id + ")'>Oui</button>";
    alert += "<button type='button' class='btn btn-danger' onclick='cancelDelete()'>Non</button></p></div>";
    $("#top-alerts").append(alert);
}

function doDelete(id) {
    //On retire la modale de confirmation
    $("#top-alerts").empty();
    //Suppression en BDD
    $.ajax({
        type: "GET",
        url: "mails_controler.php",
        data: {a: 'delete_req', id: id},
        async: true,
        success: function(data) {
            //On recharge la liste des requètes
            getRequests();
        },
    });
}

function cancelDelete() {
    $("#top-alerts").empty();
}