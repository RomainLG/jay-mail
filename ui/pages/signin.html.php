<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../ui/ico/favicon.png">

    <title><?php echo $titre ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../ui/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../ui/css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../ui/js/html5shiv.js"></script>
      <script src="../ui/js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <form class="form-signin" action="login.php" method="post">
        <img src="../ui/img/circleJay300.png"/>
        <h2 class="form-signin-heading">Veuillez vous identifier :</h2>
        <input type="text" class="form-control" value="<?php echo $login; ?>" 
               name="form_login" placeholder="Identifiant" required autofocus>
        <input type="password" class="form-control"
               name="form_pass" placeholder="Mot de passe" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
      </form>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
