<?php include("../ui/fragments/header.html"); ?>

<div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <p class="text-center">
            <?php echo "Bonjour " . $auth->getLogin(); ?>
        </p>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Recherche</h3>
            </div>
            <div class="panel-body">
                <?php include("../ui/fragments/search_form.html"); ?>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Enregistrer la recherche</h3>
            </div>
            <div class="panel-body">
                <form class="search_mail form-horizontal">
                    <div class="form-group">
                        <label for="saved-request-name">Nom</label>
                        <input type="text" name="saved-request-name" class="form-control" id="saved-request-name" required>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-default" onclick="saveRequest()">Enregistrer</button>
                    </div>
                </form>
                <p>Requètes enregistrées :</p>

                <table id="list-requests" class='table table-striped table-hover'>

                </table>
            </div>
        </div>
    </div>

    <!-- Page content -->
    <div id="page-content-wrapper">
        <div id="top-alerts"></div>
        <!--  Affichage des mails correspondants à la requète  -->
        
        <div class="view-table">
            <section id="lists_container">
            </section>
        </div>
    </div>

</div> <!-- /container -->

<?php include("../ui/fragments/modale_lecture.html") ?>        
<!-- Bootstrap core JavaScript
        ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../ui/js/jquery.js"></script>
<script src="../ui/js/jquery-ui-1.10.4.custom.js"></script>
<script src="../ui/js/bootstrap.min.js"></script>
<script src="../ui/js/modernizr.custom.09302.js"></script>

<script>
    nb_view = 0;
    nb_col = 0;
    active_view = "view0";
    list_of_views = Array();
    formToAJAX();
    addCol();
    getRequests();
    refreshAllViews();
    countUnseen();
    

    var initDatepicker = function() {
    $('input[type=date]').each(function() {
        var $input = $(this);
        $input.datepicker({
            minDate: $input.attr('min'),
            maxDate: $input.attr('max'),
            dateFormat: 'yy-mm-dd'
        });
    });
};

    if(!Modernizr.inputtypes.date){
        $(document).ready(initDatepicker);
    };

    //Custom JavaScript for the Menu Toggle
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
    });
</script>




<?php include("../ui/fragments/footer.html"); ?>