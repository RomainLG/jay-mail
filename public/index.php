<?php

require_once("../config/config.php");
$squelette = "../ui/pages/home.html.php";
$titre = "Jay Mail";
$c = "";

//Vérification de connexion
session_start("mail");
$auth = Auth::getInstance();
//var_dump($auth);

try {
    if (!($auth->estConnecte())) {
        //redirection vers le formulaire de connexion
        header("Location: ../login/login.php");
    }
} catch (Exception $e) {
    $c = $e->getMessage();
    $c .= "<pre> OOPS !" . $e->getTraceAsString() . "</pre>";
    echo $c;
    exit;
}

$gmail['user'] = $auth->getLogin();
$gmail['password'] = $auth->getPass();


$action = isset($_GET["a"]) ? $_GET["a"] : "home";
switch ($action) { 
    case "home":
        break;
    
    case "read":
        $id = $_GET["id"];
        $squelette = "../ui/pages/read.html.php";
        
        $inbox = new Mail_Inbox($gmail);        
        $message = $inbox->getMessage($id);
        
        $m = array();    
        $m['number'] = $id;
        $m['seen'] = ($message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)) ? 'true' : 'false';
        $m['subject'] = utf8_encode($message->subject);
        //$m['from'] = $message->getFrom();
        $m['from'] = utf8_encode($message->from);
        $m['to'] = $message->to;
        if (isset($message->cc)) {
            $m['cc'] = $message->cc;
        }
        $m['date'] = date("j m Y \à H\hi", strtotime($message->date));
        //$m['body'] = $message->getText();
        $m['body'] = $message->getText();
        
        break;
    
    case "test_decoup":        
        break;

    case "quitter":
        $auth->quitter();
        header("Location: " . BASE_URL . "login/login.php");
        break;

    default:
        break;
}

ob_start();
require_once($squelette);
$html = ob_get_contents();
ob_end_clean();
echo $html;
?>