<?php

require_once("../config/config.php");
session_start("mail");
$auth = Auth::getInstance();

$username = $auth->getLogin();
$password = $auth->getPass();
/*
  $gmail = '{imap.gmail.com:993/imap/ssl}INBOX';
  $yahoo = 'imap.mail.yahoo.com:993/imap/ssl}INBOX';
  $aol = 'imap.aol.com:993/imap/ssl}INBOX';
 */

$gmail['user'] = $username;
$gmail['password'] = $password;

$action = $_GET['a'];

switch ($action) {

    case 'recherche':
        $page = $_GET['page'] - 1;
        $mail_par_page = 15;
        //Ouverture de la boite
        $inbox = new Mail_Inbox($gmail);
        //Recherche
        $req = $_GET['req'];    
        $res = $inbox->search($req);
        $recent = array_reverse($res);
        //Récupération des mails
        $ms = array();
        $display_from = min(count($recent), $mail_par_page * $page);
        $display_to = min(count($recent), $display_from + $mail_par_page);
        for ($i = $display_from; $i < $display_to; $i++){
            $id = $recent[$i];
        //foreach ($res as $id) {            
            $message = $inbox->getMessage($id);
            $m = array();
            //var_dump($message);
            $m['number'] = $id;
            $m['seen'] = ($message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)) ? 'true' : 'false';
            $m['subject'] = utf8_encode($message->subject);
            //$m['from'] = $message->getFrom();
            $m['from'] = utf8_encode($message->from);
            $m['to'] = $message->to;
            if (isset($message->cc)) {
                $m['cc'] = $message->cc;
            }
            $m['date'] = date("j m Y \à H\hi", strtotime($message->date));
            $ms[] = $m;
        }
        $ret = array();
        $ret['msgs'] = $ms;
        $ret['page_max'] = count($res)/$mail_par_page;
        $json = json_encode($ret);
        echo $json;

        break;

    case "lire":
        //Affichage d'un message        
        $id = $_GET['num'];
        $inbox = new Mail_Inbox($gmail);
        $message = $inbox->getMessage($id);

        $m = array();
        $m['number'] = $id;
        $m['seen'] = ($message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)) ? 'true' : 'false';
        $m['subject'] = utf8_encode($message->subject);
        //$m['from'] = $message->getFrom();
        $m['from'] = utf8_encode($message->from);
        $m['to'] = $message->to;
        if (isset($message->cc)) {
            $m['cc'] = $message->cc;
        }
        $m['date'] = date("j m Y \à H\hi", strtotime($message->date));
        $m['body'] = $message->getText();
        //$m["body"] = $message->getContent();

        echo json_encode($m);
        break;
     
    case "count":
        //Compte le nombre de mails non lus        
        $inbox = new Mail_Inbox($gmail);
        $i = 0;
        foreach ($inbox as $message) {
            if (!$message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)) {
                $i++;
            }
        }
        echo $i;
        break;
    
    case "enregistrer_req":
        //Enregistrement d'une requte de recherche en base de données
        $r = array();
        if (isset($_GET["name"]) && $_GET["name"] != "" ){
            $req = array();
            $req["name"] = $_GET["name"];
            $req["uid"] = $auth->getId();
            $req["req"] = $_GET["req"];
            $req["from_mail"] = isset($_GET["from_mail"]) ? $_GET["from_mail"] : "";
            $req["unseen"] = isset($_GET["unseen"]) ? $_GET["unseen"] : "";
            $req["since"] = isset($_GET["since"]) ? $_GET["since"] : "";
            $req["until"] = isset($_GET["until"]) ? $_GET["until"] : "";
            Mail_Bd::saveRqst($req);
            $r["alert"] = "alert-success";
            $r["msg"] = "OK";
        }else{
            $r["alert"] = "alert-danger";
            $r["msg"] = "Nop !";            
        }
        echo json_encode($r);
        break;
        
    case "list_req":
        echo json_encode(Mail_Bd::getRqsts($auth->getId()));
        break;
    
    case "delete_req":
        $id = isset($_GET['id']) ? $_GET['id'] : 0;
        Mail_Bd::deleteRqst($id, $auth->getId());
        break;

    default:
        break;
}
?>
